package cz.krasnyd.elk;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import javafx.event.ActionEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable{
    @FXML
    public WebView web;
    public Pane popOutMenu;
    public Label bottomInfo;
    public GridPane grid;

    private Button prevButton = new Button();
    private String prevSource = "";

    @FXML
    public void onClick(ActionEvent e){
        try {
            Button btn = (Button) e.getSource();
            prevButton.getStyleClass().remove("choiced");
            prevButton = btn;
            btn.getStyleClass().add("choiced");;

            String id = btn.getId();
            System.out.println(id);
            Pane pane;
            pane = FXMLLoader.load(Main.class.getResource("ukoly/" + id + ".fxml"));
            prevSource = "ukoly/" + id + ".fxml";
            popOutMenu.getChildren().removeAll(popOutMenu.getChildren());
            popOutMenu.getChildren().addAll(pane);
            popOutMenu.setVisible(true);
        } catch (NullPointerException | IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void resetPane(){
        try {
            if (!prevSource.equals("")) {
                Pane pane;
                pane = FXMLLoader.load(Main.class.getResource(prevSource));
                popOutMenu.getChildren().removeAll(popOutMenu.getChildren());
                popOutMenu.getChildren().addAll(pane);
                popOutMenu.setVisible(true);
            }
        } catch (NullPointerException | IOException ex){
            ExceptionDialog.main(ex);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        bottomInfo.setText("© David Krásný; Verze: 1.0");
    }
}