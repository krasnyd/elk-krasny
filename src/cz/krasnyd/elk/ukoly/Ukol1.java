package cz.krasnyd.elk.ukoly;

import cz.krasnyd.elk.ExceptionDialog;
import cz.krasnyd.elk.General;
import cz.krasnyd.elk.Main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Stream;

public class Ukol1 implements Initializable{
    @FXML
    public TextField delka;
    public TextField prumer;
    public TextField zavity;
    public TextField proud;
    public TextField permeabilita;

    public Button intenzita;
    public Button indukce;
    public Button tok;

    public GridPane root;

    private int rowIndex = 0;
    private WebView web;
    private String content;
    private WebEngine webEngine;
    private int rowHeight = 50;

    private static double fm;
    private static double h;
    private static double b;
    private static double s;
    private static double fi;


    @FXML
    public void rozcestnik(ActionEvent e){
        if (e.getSource().equals(intenzita)) {
            root.getChildren().removeAll(root.getChildren());
            vypocetFm();
            rowIndex = 0;
        } else if (e.getSource().equals(indukce)){
            root.getChildren().removeAll(root.getChildren());
            vypocetIndukce();
            rowIndex = 0;
        } else if (e.getSource().equals(tok)){
            root.getChildren().removeAll(root.getChildren());
            vypocetTok();
            rowIndex = 0;
        }
    }

    private void vypocetFm(){
        if (General.getValue(zavity) <= 0 || General.getValue(proud) <= 0 || General.getValue(delka) <= 0) {
            General.chybejiciHodnota();
        } else {
            String rovnice;
            fm = General.getValue(zavity) * General.getValue(proud);
            rovnice = "Fm=N*I=" + General.getValueS(zavity) + "*" + General.getValueS(proud) + "=\\mathbf{" + General.getValueS(fm) + "}[A]";

            vykresliObrazek(rovnice);

            h = fm / General.getValue(delka);
            rovnice = "H=\\frac{Fm}{l}=\\frac{" + General.getValueS(fm) + "}{" + General.getValueS(delka) + "}=\\mathbf{" + General.getValueS(h) + "}[A*m^{-1}]";

            vykresliObrazek(rovnice);
        }
    }
    private void vypocetIndukce(){
        if (General.getValue(permeabilita) <= 0 || General.getValue(zavity) <= 0 || General.getValue(proud) <= 0 || General.getValue(delka) <= 0) {
            General.chybejiciHodnota();
        } else {
            vypocetFm();
            b = 4 * Math.PI * Math.pow(10, -7) * h * General.getValue(permeabilita);
            String rovnice;
            rovnice = "B=\\mu_{r}*\\mu_{0}*H="+ General.getValueS(permeabilita)+"*4*\\pi*10^{-7}*" + General.getValueS(h) + "=\\mathbf{" + General.getValueS(b) + "}[T]";
            vykresliObrazek(rovnice);
        }
    }
    private void vypocetTok(){
        if (General.getValue(prumer) <= 0 || General.getValue(permeabilita) <= 0 || General.getValue(zavity) <= 0 || General.getValue(proud) <= 0 || General.getValue(delka) <= 0) {
            General.chybejiciHodnota();
        } else {
            vypocetIndukce();
            General.getValueS(prumer);
            s = (Math.PI*Math.pow(Double.valueOf(General.cislo), 2)*Math.pow(10, General.kolikatou))/4;
            String rovnice; // TODO předělat na global proměnou

            if (General.kolikatou == 0) {
                rovnice = "S=\\frac{\\pi*d^{2}}{4}=\\frac{\\pi*"+ General.cislo+"^{2}}{4}=\\mathbf{"+ General.getValueS(s)+"}[m^2]";
            }
            else {
                rovnice = "S=\\frac{\\pi*d^{2}}{4}=\\frac{\\pi*"+ General.cislo+"^{2}*10^{"+ General.kolikatou+"}}{4}=\\mathbf{"+ General.getValueS(s)+"}[m^{2}]";
            }
            vykresliObrazek(rovnice);

            fi = b * s;
            rovnice = "\\phi=B*S="+ General.getValueS(b)+"*"+ General.getValueS(s)+"=\\mathbf{"+ General.getValueS(fi)+"}[Wb]";
            vykresliObrazek(rovnice);


        }
    }

    private void vykresliObrazek(String rovnice){
        try {
            web = new WebView();
            web.setMaxHeight(rowHeight);
            content = General.getHTML(rovnice);
            webEngine = web.getEngine();
            webEngine.loadContent(content);
            final com.sun.webkit.WebPage webPage = com.sun.javafx.webkit.Accessor.getPageFor(webEngine);
            webPage.setBackgroundColor(0);
            root.add(web, 0, rowIndex);
            rowIndex++;
        } catch (NullPointerException ex){
            ExceptionDialog.main(ex);
        }
    }

    @FXML
    public ImageView obrazek1;
    public ImageView civka;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            URL obr = Main.class.getResource("resources/CodeCogsEqn.gif");
            Image image = new Image(obr.toString());
            obrazek1.setImage(image);

            URL civkaURL = Main.class.getResource("resources/civka.png");
            image = new Image(civkaURL.toString());
            double pomer = 0.4;
            civka.setFitHeight(image.getHeight() * pomer);
            civka.setFitWidth(image.getWidth() * pomer);
            civka.setImage(image);

            intenzita.setOnMouseEntered(e -> {
                Stream.of(proud, delka, zavity).forEach(button -> button.setStyle("-fx-text-box-border: red ;-fx-focus-color: red ;"));
            });
            intenzita.setOnMouseExited(e -> {
                Stream.of(proud, delka, zavity).forEach(button -> button.setStyle(null));
            });

            indukce.setOnMouseEntered(event -> {
                Stream.of(proud, delka, permeabilita, zavity).forEach(button -> button.setStyle("-fx-text-box-border: red ;-fx-focus-color: red ;"));
            });
            indukce.setOnMouseExited(e -> {
                Stream.of(proud, delka, permeabilita, zavity).forEach(button -> button.setStyle(null));
            });

            tok.setOnMouseEntered(event -> {
                Stream.of(proud, delka, permeabilita, zavity, prumer).forEach(button -> button.setStyle("-fx-text-box-border: red ;-fx-focus-color: red ;"));
            });
            tok.setOnMouseExited(e -> {
                Stream.of(proud, delka, permeabilita, zavity, prumer).forEach(button -> button.setStyle(null));
            });
        } catch (NullPointerException e){
            ExceptionDialog.main(e);
        }
    }


}
