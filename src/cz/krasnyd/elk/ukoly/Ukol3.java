package cz.krasnyd.elk.ukoly;

import cz.krasnyd.elk.ExceptionDialog;
import cz.krasnyd.elk.General;
import cz.krasnyd.elk.Main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.net.URL;
import java.util.ResourceBundle;

public class Ukol3 implements Initializable{
    private final double maxF = 10e10;

    @FXML
    public TextField c1Min;
    public TextField c1Max;
    public TextField cMin;

    public Button kondenzatory;

    public GridPane root;

    private int rowIndex = 0;
    private WebView web;
    private String content;
    private WebEngine webEngine;
    private int rowHeight = 50;

    private static double c2;
    private static double cMax;

    @FXML
    public void rozcestnik(ActionEvent e){
        if (e.getSource().equals(kondenzatory)) {
            root.getChildren().removeAll(root.getChildren());
            vypocetKapacit();
            rowIndex = 0;
        }
    }

    private void vypocetKapacit(){
        if (General.getValue(c1Min)<=0 || General.getValue(c1Max)<=0 || General.getValue(cMin)<=0){
            General.chybejiciHodnota();
        } else if (General.getValue(c1Min) >= maxF || General.getValue(c1Max) >= maxF || General.getValue(cMin) >= maxF) {
            General.showError("Příliš velká hodnota", "Některá zadaná hodnota je příliš velká. Běžná kapacita kondenzátorů se pohybuje v pF až mF.");
        } else if (General.getValue(c1Min) >= General.getValue(c1Max)) {
            General.showError("Chyba: Minimum-maximum", "Minimální číslo musí být menší než maximální.");
        } else if (General.getValue(cMin) >= General.getValue(c1Min)){
            General.showError("Chyba: Cílová kapacita", "Požadovaná kapacita musí být menší než minimální kapacita C1");
        } else {
            c2 = (General.getValue(c1Min)* General.getValue(cMin))/(General.getValue(c1Min)- General.getValue(cMin));
            cMax = (General.getValue(c1Max)* c2)/(General.getValue(c1Max)+ c2);
            System.out.println("KapC2: "+ c2);
            System.out.println("maxKapacita: "+cMax);

            String rovnice;
            rovnice = "C=\\frac{C_{1} * C_{2}}{C_{1} + C_{2}}\\Rightarrow";
            root.add(new Label("Obecná rovnice:"), 0, rowIndex);
            rowIndex++;
            vykresliObrazek(rovnice);

            rovnice = "C_{2}=\\frac{C_{1min}*C_{min}}{C_{1min}-C_{min}}=\\frac{"+ General.getValueS(c1Min)+"*"+ General.getValueS(cMin)+"}{"+ General.getValueS(c1Min)+"-"+ General.getValueS(cMin)+"}=\\mathbf{"+ General.getValueS(c2)+"}[pF]";
            System.out.println(rovnice);
            vykresliObrazek(rovnice);

            rovnice = "C_{max}=\\frac{C_{1max}*C_{2}}{C_{1max}+C_{2}}=\\frac{"+ General.getValueS(c1Max)+"*"+ General.getValueS(c2)+"}{"+ General.getValueS(c1Max)+"+"+ General.getValueS(c2)+"}=\\mathbf{"+ General.getValueS(cMax)+"}[pF]";
            System.out.println(rovnice);
            vykresliObrazek(rovnice);
        }
    }

    private void vykresliObrazek(String rovnice){
        try {
            web = new WebView();
            web.setMaxHeight(rowHeight);
            content = General.getHTML(rovnice);
            webEngine = web.getEngine();
            webEngine.loadContent(content);
            final com.sun.webkit.WebPage webPage = com.sun.javafx.webkit.Accessor.getPageFor(webEngine);
            webPage.setBackgroundColor(0);
            root.add(web, 0, rowIndex);
            rowIndex++;
        } catch (Exception ex){
            ExceptionDialog.main(ex);
        }
    }

    @FXML
    public ImageView obrazek1;
    public ImageView kondenzator;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            URL obr = Main.class.getResource("resources/CodeCogsEqn.gif"); //Eqn
            Image image = new Image(obr.toString());
            obrazek1.setImage(image);

            URL kondenzatorURL = Main.class.getResource("resources/kondenzatory.png");
            image = new Image(kondenzatorURL.toString());
            double pomer = 0.5;
            kondenzator.setFitHeight(image.getHeight() * pomer);
            kondenzator.setFitWidth(image.getWidth() * pomer);
            kondenzator.setImage(image);
        } catch (NullPointerException e){
            ExceptionDialog.main(e);
        }
    }
}
