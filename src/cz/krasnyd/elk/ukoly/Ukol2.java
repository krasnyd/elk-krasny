package cz.krasnyd.elk.ukoly;

import cz.krasnyd.elk.ExceptionDialog;
import cz.krasnyd.elk.General;
import cz.krasnyd.elk.Main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.net.URL;
import java.util.ResourceBundle;

public class Ukol2 implements Initializable{
    @FXML
    public TextField permeabilita;

    public Button relativni;

    public GridPane root;

    private int rowIndex = 0;
    private WebView web;
    private String content;
    private WebEngine webEngine;
    private int rowHeight = 50;

    private static double permR;


    @FXML
    public void rozcestnik(ActionEvent e){
        if (e.getSource().equals(relativni)) {
            root.getChildren().removeAll(root.getChildren());
            vypocetPermR();
            rowIndex = 0;
        }
    }

    private void vypocetPermR(){
        if (General.getValue(permeabilita) <= 0) {
            General.chybejiciHodnota();
        } else if (General.getValue(permeabilita) >= 0.1){
            General.showError("Příliš velká permeabilita", "Zadaná hodnota je příliš velká, na to aby byla vyrobena z běžných materiálů. Ujistěte se, že jste zadali permabilitu správně. ");
        } else {
            String rovnice;
            permR = General.getValue(permeabilita) / (4*Math.PI*Math.pow(10,-7));
            rovnice = "\\mu_{r}=\\frac{\\mu}{\\mu_{0}}=\\frac{" + General.getValueS(permeabilita) + "}{4*\\pi*10^{-7}}=\\mathbf{" + General.getValueS(permR) + "}";
            vykresliObrazek(rovnice);

            String typLatky = "";
            if (permR < 1) typLatky = "diamagnetický";
            else if (permR >= 100) typLatky = "feromagnetický";
            else typLatky = "paramagnetický";


            Label jakaJe = new Label("Typ materiálu: "+typLatky);
            root.add(jakaJe, 0, rowIndex);
            rowIndex++;
        }
    }

    private void vykresliObrazek(String rovnice){
        try {
            web = new WebView();
            web.setMaxHeight(rowHeight);
            content = General.getHTML(rovnice);
            webEngine = web.getEngine();
            webEngine.loadContent(content);
            final com.sun.webkit.WebPage webPage = com.sun.javafx.webkit.Accessor.getPageFor(webEngine);
            webPage.setBackgroundColor(0);
            root.add(web, 0, rowIndex);
            rowIndex++;
        } catch (NullPointerException ex){
            ExceptionDialog.main(ex);
        }
    }

    @FXML
    public ImageView obrazek1;
    public ImageView tabulka;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            URL obr = Main.class.getResource("resources/CodeCogsEqn.gif");
            Image image = new Image(obr.toString());
            obrazek1.setImage(image);

            URL tabulkaURL = Main.class.getResource("resources/tabulka.PNG");
            image = new Image(tabulkaURL.toString());
            double pomer = 1;
            tabulka.setFitHeight(image.getHeight() * pomer);
            tabulka.setFitWidth(image.getWidth() * pomer);
            tabulka.setImage(image);
        } catch (NullPointerException e){
            ExceptionDialog.main(e);
        }
    }


}
