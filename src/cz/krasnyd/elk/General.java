package cz.krasnyd.elk;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;

import java.text.DecimalFormat;

public class General {
    public static double getValue(TextField field){
        String text = field.getText();
        try {
            text = text.replaceAll(",", ".");
            return Double.valueOf(text);
        } catch (NumberFormatException ex){
            return 0;
        }

    }

    public static String cislo = "";
    public static int kolikatou = 0;
    public static String getValueS(TextField field){
        double a = getValue(field);
        double pozmene = a;
        int nakolikatou = 0;
        if (a < 0.01){
            do {
                pozmene = pozmene * 10;
                nakolikatou--;
            } while(pozmene<1);
            DecimalFormat format = new DecimalFormat("0.##");
            cislo = format.format(pozmene);
            kolikatou = nakolikatou;
            return format.format(pozmene) + "*10^{"+nakolikatou+"}";
        }
        DecimalFormat format = new DecimalFormat("0.##");
        cislo = format.format(a);
        kolikatou = 0;
        return format.format(a);    // TODO bude vracet pouze string, který si bude přdělávat do formy, kteoru potebuje převodník obrázků
    }

    public static String getValueS(double number){
        double a = number;
        double pozmene = a;
        int nakolikatou = 0;
        if (a < 0.1){
            do {
                nakolikatou = nakolikatou + 3;
                pozmene = a * Math.pow(10, nakolikatou);
            } while(pozmene<1);
            DecimalFormat format = new DecimalFormat("0.##");
            return format.format(pozmene) + "*10^{"+(0-nakolikatou)+"}";
        }
        DecimalFormat format = new DecimalFormat("0.##");
        return format.format(number);
    }

    public static String getHTML(String rovnice){
        try {
            String zaklad =
                    "<!DOCTYPE html>\n" +
                            "<meta charset=\"utf-8\">\n" +
                            "<link rel=\"stylesheet\" href=\"" + Main.class.getResource("resources/katex.min.css") + "\">\n" +
                            "<script src=\"" + Main.class.getResource("resources/katex.min.js") + "\"></script>\n" +
                            "<body style='overflow-x: hidden;overflow-y: hidden'><div class=\"example tex\" style=\"font-size: 20px;text-align: left;\" data-expr=\"" + rovnice + "\"></div>\n" +
                            "<script>\n" +
                            "  window.onload = function() {\n" +
                            "      var tex = document.getElementsByClassName(\"tex\");\n" +
                            "      Array.prototype.forEach.call(tex, function(el) {\n" +
                            "          katex.render(el.getAttribute(\"data-expr\"), el);\n" +
                            "      });\n" +
                            "  };\n" +
                            "</script></body>\n";
            return zaklad;
        } catch (NullPointerException ex){
            ExceptionDialog.main(ex);
        }
        return "";
    }

    public static void chybejiciHodnota(){
        showError("Chybný vstup", "Některá z hodnot, nezbytných pro vyřešení problému, nebyla zadána nebo byla zadána chybně.");
    }

    public static void showError(String nazev, String obsah){
        Alert alert = new Alert(Alert.AlertType.ERROR, obsah, ButtonType.OK);
        alert.setHeaderText(nazev);
        alert.showAndWait();
    }
}
